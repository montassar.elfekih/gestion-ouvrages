export class Client {
    Id:string;
    Adresse:string;
    Nom:string;
    Prenom:string;
    Tel:string;

    constructor(Id:string, Adresse:string, Nom:string, Prenom:string,
        Tel:string){
       this.Id=Id;
       this.Adresse=Adresse;
       this.Nom=Nom;
       this.Prenom= Prenom;
       this.Tel=Tel;
   }
}

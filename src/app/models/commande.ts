import { CommandeDetail } from './commande-detail';

export class Commande {
    id: string;
    Date_commande:Date;
    id_client: string;
    etat: string;
    montant: number;
    details: CommandeDetail;
    
    public Commande( id: string,Date_commande:Date, id_client: string, etat: string,
        montant: number,details: CommandeDetail) {
    
    this.Date_commande = Date_commande
    this.id_client = id_client;
    this.etat = etat;
    this.montant = montant;
    this.details = details;
}
}

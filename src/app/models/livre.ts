export class Livre {
    title:String;
    author:String;
    price:Number;
    publishingDate:Date;
    available:Boolean;
    quantity:Number;

    constructor(title:String, author:String, price:Number, publishingDate:Date,
         available:Boolean,quantity:Number){
        this.title=title;
        this.author=author;
        this.price=price;
        this.publishingDate=publishingDate;
        this.available= available;
        this.quantity=quantity;
    }
}


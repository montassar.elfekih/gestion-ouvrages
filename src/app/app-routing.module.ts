import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListerCategoriesComponent } from './interfaces/categories/lister-categories/lister-categories.component';
import { ListerClientsComponent } from './interfaces/clients/lister-clients/lister-clients.component';
import { ListerCommandesComponent } from './interfaces/commandes/lister-commandes/lister-commandes.component';
import { ListerLivresComponent } from './interfaces/livres/lister-livres/lister-livres.component';

const routes: Routes = [
  {path: "" , component: ListerLivresComponent },
  {path: "categorie" , component: ListerCategoriesComponent },
  {path: "client" , component: ListerClientsComponent },
  {path: "commande" , component: ListerCommandesComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

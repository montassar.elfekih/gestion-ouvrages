import { Observable } from 'rxjs';

export interface CrudOperation  <T,ID> {
    save(t:T):Observable<T>;
    update(t:T): Observable<T>;
    delete(id:ID):Observable<any>;
    findall():Observable<T[]>;
    findOne(id:ID):Observable<T>;
    //findall():Observable<any[]>;
}

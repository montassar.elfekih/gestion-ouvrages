import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Commande } from '../models/commande';
import { CurdService } from './crud.service';

@Injectable({
  providedIn: 'root'
})
export class CommandeService extends CurdService <Commande , number> {

  constructor(_http :HttpClient) {
    super(_http , "http://localhost:8181/commandes");
  }

  findallA() {
    return this._http.get("http://localhost:8181/commandes");
}

}

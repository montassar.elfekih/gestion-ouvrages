import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Livre } from '../models/livre';
import { CurdService } from './crud.service';

@Injectable({
  providedIn: 'root'
})
export class LivreService extends CurdService <Livre , number> {

  constructor(_http :HttpClient) {
    super(_http , "http://localhost:8700/books");
  }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Categorie } from '../models/categorie';
import { CurdService } from './crud.service';

@Injectable({
  providedIn: 'root'
})
export class CategorieService extends CurdService <Categorie , string> {

  constructor(_http :HttpClient) {
    super(_http , "http://127.0.0.1:5000/category");
  }
}
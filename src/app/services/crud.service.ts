
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { CrudOperation } from './crud-operation';

export abstract class  CurdService <T , ID> implements CrudOperation < T, ID> {
    constructor ( protected _http : HttpClient , protected _base : string){}

    save(t: T): Observable<T> {
        return this._http.post<T>(this._base , t);
        
    }
    delete(id: ID): Observable<T> {
      return this._http.delete<T> (this._base+ '/'+id);
    }
    findall():Observable<T[]> {
        return this._http.get<T[]>(this._base);
    }
    findOne(id: ID): Observable<T> {
        return this._http.get<T>(this._base+'/'+id);
    }

    update(t: T):Observable<any> {
        return this._http.put<T>(this._base ,t,{});
    }
}

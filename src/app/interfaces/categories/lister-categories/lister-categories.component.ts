import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Categorie } from 'src/app/models/categorie';
import { CategorieService } from 'src/app/services/categorie.service';

@Component({
  selector: 'app-lister-categories',
  templateUrl: './lister-categories.component.html',
  styleUrls: ['./lister-categories.component.css']
})
export class ListerCategoriesComponent implements OnInit {
  categories: Categorie[]
  constructor(private categorieService:CategorieService, private router:Router) { }
  ngOnInit(): void {
    this.afficherCategorie() ;
  }
  afficherCategorie() {
    this.categorieService.findall().subscribe(data=>{
      this.categories=data;
      //this.livres=data;
      console.log(this.categories);
    },err=>{ console.log(err);

    })
  }

  OnChercher(form:any){
    // this.mot_cle= form.mot_cle;
    // this.universites= this.universitiesall;
    // {
    //     if (this.mot_cle !=""){
    //         this.universites=this.universites.filter(x=>x.nomUNV==form.mot_cle);

    //     }

    //     else
    //     this.universites= this.universitiesall;

    // }

  }

}

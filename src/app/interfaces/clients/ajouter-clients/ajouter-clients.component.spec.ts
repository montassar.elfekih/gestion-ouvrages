import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AjouterClientsComponent } from './ajouter-clients.component';

describe('AjouterClientsComponent', () => {
  let component: AjouterClientsComponent;
  let fixture: ComponentFixture<AjouterClientsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AjouterClientsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AjouterClientsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

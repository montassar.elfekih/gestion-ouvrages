import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListerCommandesComponent } from './lister-commandes.component';

describe('ListerCommandesComponent', () => {
  let component: ListerCommandesComponent;
  let fixture: ComponentFixture<ListerCommandesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListerCommandesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListerCommandesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

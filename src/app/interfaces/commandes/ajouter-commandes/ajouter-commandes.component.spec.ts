import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AjouterCommandesComponent } from './ajouter-commandes.component';

describe('AjouterCommandesComponent', () => {
  let component: AjouterCommandesComponent;
  let fixture: ComponentFixture<AjouterCommandesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AjouterCommandesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AjouterCommandesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Livre } from 'src/app/models/livre';
import { LivreService } from 'src/app/services/livre.service';

@Component({
  selector: 'app-lister-livres',
  templateUrl: './lister-livres.component.html',
  styleUrls: ['./lister-livres.component.css']
})
export class ListerLivresComponent implements OnInit {

  public livres:Livre[];
  constructor(private livresService:LivreService, private router:Router) { }

  ngOnInit(): void {
    this.afficherLivres() ;
  }
  afficherLivres() {
    this.livresService.findall().subscribe(data=>{
      this.livres=data;
      this.livres=data;
      console.log(this.livres);
    },err=>{ console.log(err);

    })
  }

  OnChercher(form:any){
    // this.mot_cle= form.mot_cle;
    // this.universites= this.universitiesall;
    // {
    //     if (this.mot_cle !=""){
    //         this.universites=this.universites.filter(x=>x.nomUNV==form.mot_cle);

    //     }

    //     else
    //     this.universites= this.universitiesall;

    // }

  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListerLivresComponent } from './lister-livres.component';

describe('ListerLivresComponent', () => {
  let component: ListerLivresComponent;
  let fixture: ComponentFixture<ListerLivresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListerLivresComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListerLivresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
